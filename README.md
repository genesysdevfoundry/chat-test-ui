# Chat Test UI #

An HTML/Javascript application that can be used to test both Genesys Chat API v1 (GME Chat) and v2 (GWE Chat).

### How do I get set up? ###

* Download the source code
* Modify the URL in the gmschat.html file to point to your GMS server 
* Host the files on your web server
* Navigate to the gmschat.html file
* Enjoy ;)

### To report issues or ask questions? ###

* Post to the Questions section of the Genesys DevFoundry (https://developer.genesys.com)