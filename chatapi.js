/**
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
 
// ChatFactory implements a Factory methodology to create the necessary chat implementation class
// based on the Genesys Chat API type being requested.
var ChatFactory = function(config) {

	var apiObj;
	
	// Create the appropriate chat API implementation based on the config.chatType value
	// valid values are 'apiv1' and 'apiv2'
	switch(config.chatType) {
		case 'apiv1':
			apiObj = Chat.createAPIv1();
			apiObj.init(config);
			break;
			
		case 'apiv2':
			apiObj = Chat.createAPIv2();
			apiObj.init(config);
			break;
	}
	
	// Create an instance of a wrapper class that encapsulates the chat API implementation
	var chatObj = {
		_chatapi: apiObj,
		
		startChat: function(preChatSurvey) {
			this._chatapi.startChat(preChatSurvey);
		},
		
		endChat: function() {
			this._chatapi.endChat();
		},
		
		sendMessage: function(message) {
			this._chatapi.sendMessage(message);
		}
	}
	
	// Return the wrapper class to the caller
	return chatObj;
}

// IE doesn't support Object.create() so implement a version of it that will work for our needs
if (!Object.create) {  
    Object.create = function (o) {
        if (arguments.length > 1) {
            throw new Error('Object.create implementation only accepts the first parameter.');
        }
        function F() {}
        F.prototype = o;
        return new F();
    };
}

// This merges the properties of two classes together to allow for object inheritance 
var fromPrototype = function(prototype, object) {  
    var newObject = Object.create(prototype);
    for (var prop in object) {
        if (object.hasOwnProperty(prop)) {
            newObject[prop] = object[prop];
        }
    }
  	return newObject;
};

// Our base Chat class implementation to be overridden by the implementation classes
var Chat = { 
	init: function(config) {}, 
    startChat: function(preChatSurvey) {},
    endChat: function() {},
    sendMessage: function(message) {}
};

// An implementation of the Genesys Chat API v1
//
// Genesys Chat API v1 first creates an instance of a service in Genesys Orchestration
// It then creates that Chat session that will be tied to that service.
// Chat API v1 can also return events using CometD or interval Polling.  This class
// supports both event models based on the value of config.useCometD
//
// Note, this class does not implement the entire API, but just enough to show the
// basics of how the API works.
//
Chat.createAPIv1 = function() {  
    return fromPrototype(Chat, {
    	_config: {},
    	_cometdSubscription: null,
    	_serviceId: null,
    	_preChatSurvey: null,
    	_verboseParam: '',
    	
    	// Initialize the Chat API v1 Class
    	init: function(config) {
			
			var me = this;
			
			// Save off the config object for later use
    		me._config = config;
			
			// If config.verbose is true, then setup the _verboseParam to be sent along with requests
    		if ( me._config.verbose === true ) {
    			me._verboseParam = "_verbose=true&";
    		}
			
			// If CometD is being used for events, then setup the various aspects needed to work with CometD
			if ( me._config.useCometD === true ) {
			
				// Setup jQuery AJAX to send 'gmsuser' header with every request
				// 
				// Note, the documentation says to send a header named 'gms_user', however HTTP headers
				// that contain '_' characters are ignored by default by Apache, Nginx, and IIS.  Therefore
				// we'll send a header of 'gmsuser' that will get through and then use Reverse Proxy rules 
				// to convert it to the proper 'gms_user' header on the backside.
				//
				var guid = me._generateGUID();
				$.ajaxSetup({
					headers: { 
						'gmsuser': guid 
					}
				});			

				// Setup the CometD session making sure to turn off websockets so that the longpolling
				// transport is used instead.
				//
				// Note, the same 'gmsuser' header needs to be sent on the CometD requests and it will also
				// be converted to the 'gms_user' header on the backside.
				$.cometd.websocketEnabled = false;
				$.cometd.configure({
					url: me._config.baseURL + '/genesys/cometd',
					logLevel: 'debug',
					requestHeaders: {
						'gmsuser': guid
					}
				});
			}
			
			// Modify the config.baseURL to reflect the API v1 URI
			me._config.baseURL = me._config.baseURL + '/genesys/1/service';			
    	},
    	
    	// Start the Chat with the preChatSurvey values
		startChat: function(preChatSurvey) {

			var me = this;
			
			// Save off the pre-Chat Survey values so that they can be used when the chat session is created
			me._preChatSurvey = preChatSurvey;
			
			// Start the CometD event channel subscribing to the '/_genesys' topic.
			if ( me._config.useCometD == true ) {
				$.cometd.handshake(function(handshakeReply) {
					if (handshakeReply.successful) {
						me._cometdSubscription = $.cometd.subscribe("/_genesys", function(response) {
						
							// Process any updates to the transcript
							me._dispatchChatTranscript(response.data.message.transcriptToShow);
							
						});
						
						// Once the CometD Handshake has completed we can create the Orchestration service session
						me._startServiceSession();
					}
				});
			} else {
			
				//  We aren't using CometD, so just start the Orchestration service session
				me._startServiceSession();	
			}
		},
				
		// End the Chat
		endChat: function() {
			
			var me = this;
			
			// End the chat session
			me._endChatSession();
			
			// Stop the CometD channel
			if ( me._config.useCometD == true ) {
				$.cometd.unsubscribe(me._cometdSubscription);
				$.cometd.disconnect();
			}
		},

		// Send a message
		sendMessage: function(message) {
			var me = this;
						
			// Chat API v1 doesn't contain a specific 'send' request.  
			// It instead send messages using the 'refresh' request 
			me._refreshChat(message);
		},
				
		// Start the Orchestration service session
		_startServiceSession: function() {
		
			var me = this;
			
			// Populate the parameters and URL
			var params = me._verboseParam;
			var url = me._config.baseURL + '/' + me._config.chatServiceName;
			
			$.ajax({
					type: 'POST',
					url: url,
					contentType: 'application/x-www-form-urlencoded',
					data: params
			}).done(function(response, status, xhr) {
				if ( me._config.debug === true ) {
					console.log("startChat service response: ");
					console.log(response);
				}

				// Save off the service id as we need it for every subsequent request				
				me._serviceId = response._id;

				me._startChatSession();					
			}).fail(function(xhr, status, err) {
				me._config.onError('Unable to create service session: ' + xhr.responseJSON.errors[0].advice);
			});
		},
			
		// Start the chat session
		_startChatSession: function() {
		
			var me = this;

			// If we are using CometD for events, then we need to pass the 'notify_by' parameter with a value of 'cometd'			
			var cometd = "";
			if ( me._config.useCometD == true ) {
				cometd = "notify_by=comet&";
			}

			// Populate the parameters and URL
			var params = me._verboseParam + cometd + me._preChatSurvey;
			var url = me._config.baseURL + '/' + me._serviceId + '/ixn/chat';

			$.ajax({
					type: 'POST',
					url: url,
					contentType: 'application/x-www-form-urlencoded',
					data: params
			}).done(function(response, status, xhr) {
				if ( me._config.debug === true ) {
					console.log("startChat ixn/chat response: ");
					console.log(response);
				}
			
				// Save the starting transcript position
				me._transcriptPosition = response.transcriptPosition;
			
				// Let listeners know that the chat session has started successfully
				me._config.onStarted();
				
				// If we are NOT using CometD then kick off an interval object that will
				// making polling 'refresh' requests to retrieve any transcript changes
				if ( me._config.useCometD == false ) {
					me._startChatRefresh();
					me._refreshChat();
				}
			}).fail(function(xhr, status, err) {
				me._config.onError('Unable to create chat session: ' + xhr.responseJSON.message);
			});
		},
		
		// End the chat session
		_endChatSession: function() {
			
			var me = this;
			
			// Populate the parameters and URL
			var params = me._verboseParam;
			var url = me._config.baseURL + '/' + me._serviceId + '/ixn/chat/disconnect';

			$.ajax({
					type: 'POST',
					url: url,
					contentType: 'application/x-www-form-urlencoded',
					data: params
			}).done(function(response, status, xhr) {
				if ( me._config.debug === true ) {
					console.log( "endChat response: ");
					console.log(response);
				}

				// If we are NOT using CometD, then we need to clear the interval object
				// to stop the polling 'refresh' requests
				if ( me._config.useCometD == false ) {					
					me._stopChatRefresh();
				}
				
				// Clear out the service id
				me._serviceId = null;						

				// Let listeners know that the chat session has ended
				me._config.onEnded();
			}).fail(function(xhr, status, err) {
				me._config.onError('Unable to end chat session');
			});
		},
		
		// Start an interval object to make 'refresh' requests at 5 second intervals
		_startChatRefresh: function() {
		
			var me = this;
			
			me._chatRefreshIntervalId = setInterval( function() {
				me._refreshChat();
			}, 5000);
		},

		// Stop the interval object from making 'refresh' requests		
		_stopChatRefresh: function() {
		
			var me = this;
		
			clearInterval(me._chatRefreshIntervalId);
		},
		
		
		// Refresh the Chat transcript by making a 'refresh' request
		//
		// Note: this same method is used to 'send' a message from the user
		//
		_refreshChat: function(message) {
		
			var me = this;
			
			var params = me._verboseParam + 'transcriptPosition=' + me._transcriptPosition;
			
			// If a message is being sent from the user, then include it in the parameters
			if ( message !== undefined) {
				params += '&message=' + message;
			}
			
			var url = me._config.baseURL + '/' + me._serviceId + '/ixn/chat/refresh';

			$.ajax({
					type: 'POST',
					url: url,
					contentType: 'application/x-www-form-urlencoded',
					data: params
			}).done(function(response, status, xhr) {
				if ( me._config.debug === true ) {
					console.log("refreshChat response: ");
					console.log(response);
				}

				// Update the transcript position
				me._transcriptPosition = response.transcriptPosition;
			
				// If CometD is being used, then transcript updates will be received on the 
				// CometD event channel, so don't process the transcript here, otherwise we'll
				// see duplicate messages displayed to the user in the transcript
				if ( me._config.useCometD === false ) {
					if ( response.transcriptToShow !== undefined ) {
						me._dispatchChatTranscript(response.transcriptToShow);
					}
										
					// If the chat has ended, perhaps by the agent ending the chat, then
					// stop the interval object from polling for transcript updates
					if ( response.chatEnded == true ) {
						me._stopChatRefresh();
					}
				}
		
				// If we were sending a message, then let listeners know that the message was sent successfully
				if ( message !== undefined ) {
					me._config.onMessageSent(message);
				}
			}).fail(function(xhr, status, err) {
				me._config.onError('Unable to refresh chat session');
			});
		},

		// Dispatch transcript updates to the listeners
		_dispatchChatTranscript: function(transcript) {
		
			var me = this;
			
			// For each item in the transcript...
			$.each(transcript, function(index, message) {
			
				// Call the onMessageReceived of the listener with the 'type', 'nickname', and 'message'
				me._config.onMessageReceived( message[0], message[1], message[2] );
			});
		},

		// A utility function to generate a GUID to be used for the value in the 'gmsuser' header
		_generateGUID: function() {
			return (this._S4() + this._S4() + "-" + this._S4() + "-4" + this._S4().substr(0,3) + "-" + this._S4() + "-" + this._S4() + this._S4() + this._S4()).toLowerCase();
		},
		
		// A utility function to generate a random component of the GUID
		_S4: function() {
			return (((1+Math.random())*0x10000)|0).toString(16).substring(1); 
		},
    });
};

// An implementation of the Genesys Chat API v2
//
// Genesys Chat API v2 is the API implemented and used by Genesys Web Engagement
// It previously used to be exposed by a component known at Genesys WebAPI Server,
// but is now hosted by GMS.
//
// It differs from Chat API v1 in that no Orchestration session is created, and it
// DOES NOT offer a CometD event channel.
//
// Note, this class does not implement the entire API, but just enough to show the
// basics of how the API works.
//
Chat.createAPIv2 = function(config) {  
    
    return fromPrototype(Chat, {
    	_config: {},
    	_chatId: null,
    	_userId: null,
    	_secureKey: null,
    	_alias: null,
    	_transcriptPosition: 1,
    	_chatRefreshIntervalId: null,
    	
    	// Initialize the Chat API v2 Class
    	init: function(config) {
    		var me = this;
			
			// Save off the config object for later use
			me._config = config;
			
			// Modify the config.baseURL to reflect the API v2 URI
			me._config.baseURL = me._config.baseURL + '/genesys/2';
    	},
    	
    	// Start the Chat with the preChatSurvey values
        startChat: function(preChatSurvey) {
        
        	var me = this;
        	
        	var url = me._config.baseURL + '/chat/' + me._config.chatServiceName;

			$.ajax({
					type: 'POST',
					url: url,
					contentType: 'application/x-www-form-urlencoded',
					data: preChatSurvey
			}).done(function(response, status, xhr) {
				if ( me._config.debug === true ) {
					console.log("startChat response: ");
					console.log(response);
				}
				
				// The four values are required for subsequent requests
				me._chatId = response.chatId;
				me._userId = response.userId;
				me._secureKey = response.secureKey;
				me._alias = response.alias;
				
				// Save off the transcript position
				me._transcriptPosition = 1;
				
				// Let listeners know that the chat session started successfully
				me._config.onStarted();
				
				// Start the interval polling for transcript updates
				me._startChatRefresh();
				me._refreshChat();
			}).fail(function(xhr, status, err) {
				me._config.onError('Unable to create chat session: ' + xhr.responseJSON.errors[0].advice);
			});
        },
        
        // End the chat session
        endChat: function() {
        
        	var me = this;
        
        	// Populate the parameters and URL
			var params = 'userId=' + me._userId + '&secureKey=' + me._secureKey + '&alias=' + me._alias;
			var url = me._config.baseURL + '/chat/' + me._config.chatServiceName + '/' + me._chatId + '/disconnect';

			$.ajax({
					type: 'POST',
					url: url,
					contentType: 'application/x-www-form-urlencoded',
					data: params
			}).done(function(response, status, xhr) {
				if ( me._config.debug === true ) {
					console.log( "endChat response: ");
					console.log(response);
				}
				
				// Stop the interval polling for transcript updates
				me._stopChatRefresh();
				
				// Clear out the session values
				me._chatId = response.chatId;
				me._userId = response.userId;
				me._secureKey = response.secureKey;
				me._alias = response.alias;
				me._transcriptPosition = 1;
				
				// Let the listeners know that the chat has ended
				me._config.onEnded();
			}).fail(function(xhr, status, err) {
				me._config.onError('Unable to end chat session');
			});
        },
        
        // Send a message
        sendMessage: function(message) {
        
        	var me = this;
        
        	// Populate the parameters and URL
			var params = 'message=' + message + '&userId=' + me._userId + '&secureKey=' + me._secureKey + '&alias=' + me._alias;
			var url = me._config.baseURL + '/chat/' + me._config.chatServiceName + '/' + me._chatId + '/send';

			$.ajax({
					type: 'POST',
					url: url,
					contentType: 'application/x-www-form-urlencoded',
					data: params
			}).done(function(response, status, xhr) {
				if ( me._config.debug === true ) {
					console.log("sendMessage response: ");
					console.log(response);
				}
				
				// Let listeners know that the message was sent successfully
				me._config.onMessageSent(message);
			}).fail(function(xhr, status, err) {
				me._config.onError('Unable to send message');
			});
        },
        
		// Start an interval object to make 'refresh' requests at 5 second intervals
		_startChatRefresh: function() {
			
			var me = this;
			
			me._chatRefreshIntervalId = setInterval( function() {
				me._refreshChat();
			}, 5000);
		},
		
		// Stop the interval object from making 'refresh' requests		
		_stopChatRefresh: function() {
			
			var me = this;
			
			clearInterval(me._chatRefreshIntervalId);
		},
		
		// Refresh the Chat transcript by making a 'refresh' request
		_refreshChat: function() {
		
			var me = this;
			
			var params = 'userId=' + me._userId + '&secureKey=' + me._secureKey + '&alias=' + me._alias + '&transcriptPosition=' + me._transcriptPosition;
			var url = me._config.baseURL + '/chat/' + me._config.chatServiceName + '/' + me._chatId + '/refresh';

			$.ajax({
					type: 'POST',
					url: url,
					contentType: 'application/x-www-form-urlencoded',
					data: params
			}).done(function(response, status, xhr) {
				if ( me._config.debug === true ) {
					console.log("refreshChat response: ");
					console.log(response);
				}

				// Update the transcript position
				me._transcriptPosition = response.nextPosition;
			
				// For each item in the transcript...
				$.each(response.messages, function(index, message) {
				
					// Call the onMessageReceived of the listener with the 'type', 'nickname', and 'message'
					me._config.onMessageReceived(message.type, message.from.nickname, message.text);
				});
				
				// If the chat has ended, perhaps by the agent ending the chat, then
				// stop the interval object from polling for transcript updates
				if ( response.chatEnded == true ) {
					me._stopChatRefresh();
					me._config.onEnded()
				}
			}).fail(function(xhr, status, err) {
				me._config.onError('Unable to refresh chat session');
			});
		}
    });
};
